<?php
if (!defined('WP_UNINSTALL_PLUGIN')) {
	exit;
}

function sp_database_uninstall() {
	global $wpdb;

	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sp_groups"); 
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}sp_users_groups");
	
	$wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'sp_calendar'"  );
	$wpdb->query("DELETE FROM $wpdb->usermeta WHERE meta_key = 'sp_debug'");
}
sp_database_uninstall();

?>

