<?php
/**
 * Plugin Name: Strona Przedmiotu
 * Plugin URI: https://bitbucket.org/tabasz/pt335957_strona_przedmiotu
 * Author: Paweł Tabaszewski
 * Author URI: http://students.mimuw.edu.pl/~pt335957/wordpress
 */

function sp_database_istall() {
	global $wpdb;

	$sp_groups = "{$wpdb->prefix}sp_groups";
	$sp_users_groups = "{$wpdb->prefix}sp_users_groups";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	$sql = "CREATE TABLE $sp_groups (
			id int(11) NOT NULL AUTO_INCREMENT,
			course_unit_id int(11) NOT NULL,
			group_number int(11) NOT NULL,
			course_name varchar(255) NOT NULL,
			class_type varchar(255) NOT NULL,
			PRIMARY KEY  (id),
			UNIQUE KEY (course_unit_id, group_number)
		)"; 
	dbDelta($sql);

	$sql = "CREATE TABLE $sp_users_groups (
			user_id int(11) NOT NULL,
			group_id int(11) NOT NULL,
			relationship_type varchar(255) NOT NULL,
			PRIMARY KEY  (user_id, group_id)
		)"; 
	dbDelta($sql);
}
register_activation_hook(__FILE__, 'sp_database_istall');

function sp_get_usos_calendar($user_id, $provider) {
	$response = $provider->api()->get('https://usosapps.uw.edu.pl/services/tt/upcoming_share?lang=pl');
	update_user_meta($user_id, 'sp_calendar', $response);
}

function sp_insert_into_groups($group) {
	global $wpdb;
	$sp_groups = "{$wpdb->prefix}sp_groups";
	
	$group->course_name->pl = str_replace('ó', 'o', $group->course_name->pl);
	
	$query = " FROM $sp_groups
			  WHERE course_unit_id=".$group->course_unit_id." AND ".
		 	 "group_number=".$group->group_number;
	$count = $wpdb->get_var("SELECT COUNT(*)".$query);

	if ($count == 1) {
		return 0;
	}
	
	
	$wpdb->insert(
		$sp_groups, 
		array(
			'course_unit_id' => $group->course_unit_id, 
			'group_number' => $group->group_number, 
			'course_name' => $group->course_name->pl, 
			'class_type' => $group->class_type->pl, 
		), 
		array(
			'%d', 
			'%d',
			'%s', 
			'%s'
		)			 
	);
	
	return $wpdb->get_var("SELECT id".$query);
}

function sp_insert_into_users_groups($user_id, $group_id, $relationship_type) {
	global $wpdb;
	$sp_users_groups = "{$wpdb->prefix}sp_users_groups";
	
	return $wpdb->insert(
			$sp_users_groups,
			array(
				'user_id' => $user_id, 
				'group_id' => $group_id, 
				'relationship_type' => $relationship_type, 
			), 
			array(
				'%d', 
				'%d',
				'%s'
			)			 
		);
}

function sp_insert_contact($user_id, $contact) {
	global $wpdb;
	$wsluserscontacts = "{$wpdb->prefix}wsluserscontacts";
	$wslusersprofiles = "{$wpdb->prefix}wslusersprofiles";
	
	$query = "SELECT id FROM $wsluserscontacts
			  WHERE provider='Usosweb' AND
			  user_id=".$user_id." AND ".
		 	 "identifier=".$contact->id;
		 	 
	$id = $wpdb->get_var($query);
	
	if (is_numeric($id) && $id > 0) {
		return $id;
	}
	
	$email = $contact->email;
	if ($email == '') {
		$email = $wpdb->get_var("SELECT email FROM $wslusersprofiles 
			WHERE provider='Usosweb' AND identifier=".$contact->id);
	}
	
	$wpdb->insert(
			$wsluserscontacts, 
			array(
				'user_id' => $user_id, 
				'provider' => 'Usosweb', 
				'identifier' => $contact->id,
				'full_name' => str_replace('ó', 'o', $contact->first_name).' '.
							   str_replace('ó', 'o', $contact->last_name),
				'email' => $email,
				'profile_url' => $contact->profile_url,
				'photo_url' => ''//$contact->photo_urls->50x50
			), 
			array(
				'%d', 
				'%s',
				'%d',
				'%s', 
				'%s',
				'%s',
				'%s'
			)			 
		);
	
	return $wpdb->get_var($query);
}

function sp_insert_all_contacts($user_id, $provider, $group_id, $users, $relationship_type) {
	$ids_arr = array();
	foreach ($users as $user) {
		$ids_arr[] = $user->id;
	}
	
	$n_users = $provider->api()->get("https://usosapps.uw.edu.pl/services/users/users?user_ids=".
		implode("|", $ids_arr)."&fields=id|first_name|last_name|email|profile_url");

	foreach ($n_users as $n_user) {
		$n_user_id = sp_insert_contact($user_id, $n_user);
		sp_insert_into_users_groups($n_user_id, $group_id, $relationship_type);
	}
}

function sp_get_usos_contacts($user_id, $provider) {
	$response = $provider->api()->get('https://usosapps.uw.edu.pl/services/groups/user?fields=course_name|class_type|group_number|course_unit_id');
	
	$groups = $response->groups;

	foreach ($groups as $terms) {
		foreach ($terms as $group) {
			
			$group_id = sp_insert_into_groups($group);
			if ($group_id !== 0) {
				$response = $provider->api()->get("https://usosapps.uw.edu.pl/services/groups/group?course_unit_id=".
					$group->course_unit_id."&group_number=".$group->group_number."&fields=lecturers|participants");
				
				sp_insert_all_contacts($user_id, $provider, $group_id, $response->lecturers, 'lecturer');
				sp_insert_all_contacts($user_id, $provider, $group_id, $response->participants, 'participant');
			}
		}
	}
}

function sp_usos_init($user_id, $provider, $hybridauth_user_profile)
{	
	if('Usosweb' != $provider) {
		return;
	}
		
	include_once(WORDPRESS_SOCIAL_LOGIN_ABS_PATH . '/hybridauth/Hybrid/Auth.php');
 
	try	{
		$provider = Hybrid_Auth::getAdapter('Usosweb'); 
 		
		sp_get_usos_calendar($user_id, $provider);
		sp_get_usos_contacts($user_id, $provider);
	}
	catch(Exception $e) {
		echo "Ooophs, we got an error: " . $e->getMessage();
	}
}
add_filter('wsl_hook_process_login_before_wp_set_auth_cookie', 'sp_usos_init', 10, 3);

function sp_usos_calendar_function() {
	if (get_current_user_id() == 0) {
		return;
	}
	
	$arr = get_user_meta(get_current_user_id(), 'sp_calendar');
	return do_shortcode('[weekscalendar weeks=1 '.$arr[0]->webcal_url.']');
}

add_shortcode('usos_calendar', 'sp_usos_calendar_function');

function sp_debug_function() {
	if (get_current_user_id() !== 1) {
		return;
	}
	
	$arr = get_user_meta(get_current_user_id(), 'sp_debug');
	return print("<pre>".print_r($arr,true)."</pre>");
}

add_shortcode('debug', 'sp_debug_function');

function sp_get_users($group_id, $relationship_type) {
	global $wpdb;

	$sp_users_groups = "{$wpdb->prefix}sp_users_groups";
	$wsluserscontacts = "{$wpdb->prefix}wsluserscontacts";
	
	$ret = $wpdb->get_results("SELECT u.full_name,u.email
		FROM $wsluserscontacts u, $sp_users_groups ug
		WHERE u.id=ug.user_id AND ug.group_id=".$group_id." AND ug.relationship_type='".$relationship_type."'");
	
	return $ret;
}

function sp_users_table($groups) {
	$ret = '';

	foreach ($groups as $group) {
		$lecturers = sp_get_users($group->id, 'lecturer');
		$participants = sp_get_users($group->id, 'participant');
		
		$emails = array();
		foreach ($participants as $participant) {
			if ($participant->email != '') {
				$emails[] = $participant->email;
			} 
		}
		
		$ret .= "<tr><th>".$group->course_name." ".$group->class_type.
			"</th><th><a href='mailto:".implode(",", $emails)."'>Mail do uczestników</a></th></tr>";
		
		foreach ($lecturers as $lecturer) {
			$ret .= "<tr><td>Prowadzący: ".$lecturer->full_name."</td><td>".
				"<a href='mailto:".$lecturer->email."'>".$lecturer->email."</a></td></tr>";
		}
		
		foreach ($participants as $participant) {
			$ret .= "<tr><td>".$participant->full_name."</td><td>".
				"<a href='mailto:".$participant->email."'>".$participant->email."</a></td></tr>";
		}
	}
	
	return $ret;
}

function sp_usos_contacts_function() {
	$id = get_current_user_id();
	if ($id == 0) {
		return;
	}
	
	global $wpdb;

	$sp_groups = "{$wpdb->prefix}sp_groups";
	$sp_users_groups = "{$wpdb->prefix}sp_users_groups";
	$wsluserscontacts = "{$wpdb->prefix}wsluserscontacts";
	$wslusersprofiles = "{$wpdb->prefix}wslusersprofiles";
	
	$identifier = $wpdb->get_var("SELECT identifier FROM $wslusersprofiles WHERE user_id=$id");
	$user_id = $wpdb->get_var("SELECT id FROM $wsluserscontacts WHERE identifier=$identifier");
	
	$ret = '';
	
	$count = $wpdb->get_var("SELECT COUNT(*) FROM $sp_users_groups
							 WHERE user_id=$user_id 
							 AND relationship_type='lecturer'");
	if ($count > 0) {
		$ret .= "Prowadzisz: <br /><table>";
		
		$groups = $wpdb->get_results("SELECT g.id,g.course_name,g.class_type
			FROM $sp_groups g, $sp_users_groups ug
			WHERE g.id=ug.group_id AND ug.user_id=$user_id
			AND ug.relationship_type='lecturer'");
		
		$ret .= sp_users_table($groups)."</table>";
	}
	
	$count = $wpdb->get_var("SELECT COUNT(*) FROM $sp_users_groups
							 WHERE user_id=$user_id 
							 AND relationship_type='participant'");
	
	if ($count > 0) {
		$ret .= "Uczęszczasz: <br /><table>";
		
		$groups = $wpdb->get_results("SELECT g.id,g.course_name,g.class_type
			FROM $sp_groups g, $sp_users_groups ug
			WHERE g.id=ug.group_id AND ug.user_id=$user_id
			AND ug.relationship_type='participant'");
		
		$ret .= sp_users_table($groups)."</table>";
	}
	
	echo $ret;
}
add_shortcode('usos_contacts', 'sp_usos_contacts_function');

class sp_usos_calendar extends WP_Widget {

	function wp_usos_calendar() {
		parent::WP_Widget(false, $name = __('Kalendarz USOS', 'wp_widget_plugin'));
	}

	function form($instance) {	
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		return $instance;
	}

	function widget($args, $instance) {
		echo do_shortcode('[usos_calendar]');
	}
}

add_action('widgets_init', create_function('', 'return register_widget("sp_usos_calendar");'));
?>
